FROM node:latest

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
RUN npm i -g http-server

# Bundle app source
COPY . .
RUN npm run build --prod

EXPOSE 8080
CMD [ "http-server", "-p", "8080",  "./dist"]
