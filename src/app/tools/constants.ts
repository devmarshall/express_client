const cnst = {
  API_URL: 'https://expressfromus.com/api/v1',

  URLS: {
    index: '/',
    login: 'login',
  }
};

export default cnst;
